function repeatN (n,f) {
	const node = document.createElement("li");
	if(n<=1) {
		node.innerHTML = `fibo(${n}) = ${f(n)}`;
		my_ul.appendChild(node);
	}
	else {
		repeatN(n-1,f);
		node.innerHTML = `fibo(${n}) = ${f(n)}`;
		my_ul.appendChild(node);
	}
	
}
