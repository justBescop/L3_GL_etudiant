#include <CppUTest/CommandLineTestRunner.h>
#include "Spline.hpp"

TEST_GROUP(GroupSpline) { } ;
TEST(GroupSpline, Spline_test0){
	Spline s;
	s.addKey(0, {-1, 0});
	s.addKey(1, {0, 0});
	s.addKey(2, {1, 1});
	s.addKey(3, {1, 0});
	DOUBLES_EQUAL(1, s.getStartTime(), 1e-6);
}
