#include "Fibo.hpp"
#include <cassert>
#include <string>
int fibo(int n, int f0, int f1) {
	/*On vérifie que les valeurs croissent et qu'elles soient toujours 
	*positives
	* Pour desactiver les asserts : utiliser -DNDEBUG 
	* ou cmake -DCMAKE_BUILD_TYPE=Release...*/
	/*assert(f1 >= f0);
	assert(f0 >= 0);*/
	
	if((f0<0) or (f1<f0)){
		throw std::string("nimp");
	}
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

