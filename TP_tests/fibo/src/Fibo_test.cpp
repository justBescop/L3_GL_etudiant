#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>
#include <string>
TEST_GROUP(GroupFibo){ };
TEST(GroupFibo, test_fibo_1){
	int result = fibo(1);
	CHECK_EQUAL(1,result);	
}
TEST(GroupFibo, test_fibo_2){
	int result = fibo(2);
	CHECK_EQUAL(1,result);	
}
TEST(GroupFibo, test_fibo_3){
	int result = fibo(3);
	CHECK_EQUAL(2,result);	
}
TEST(GroupFibo, test_fibo_4){
	int result = fibo(4);
	CHECK_EQUAL(3,result);	
}
TEST(GroupFibo, test_fibo_5){
	int result = fibo(5);
	CHECK_EQUAL(5,result);	
}
TEST(GroupFibo, test_fibo_6){
	try{
		int result = fibo(50);
	}
	catch(const std::string & e){
		CHECK("nimp" == e );
		}
	catch(...){
		FAIL("Erreur type exception");
	}	
}
